<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="/struts-tags" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Registration form</h2>
	<c:form action="testAction">
		<c:textfield name="firstName" label="First name" />
		<c:textfield name="lastName" label="Last name" />
		<c:radio name="gender" list="{'Male','Female'}" label="Gender" />
		<c:textfield name="age" label="Age"/>
		<c:textfield name="email" label="Email"/>
		<c:textarea name="address" cols="30" rows="7" label="Address" />
		<c:select list="colors" name="favoriteColor" label="Your favorite color" headerKey="None" headerValue="Select your favorite color"/>
		<c:checkbox name="subscription" value="true" label="Is subscription" />
		<c:checkboxlist list="manyState" name="state" />
		<c:reset value="Reset" />
		<c:submit value="submit" />
	</c:form>
	<br/><br/><br/>
<%-- 	<table>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Price</th>
		</tr>
		<c:iterator value="products" var="product" >
			<tr>
				<td>${product.id}</td>
				<td>${product.name}</td>
				<td>${product.price}</td>
			</tr>
		</c:iterator>
	</table> --%>
</body>
</html>