<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="/struts-tags" %>

<script type="text/javascript">
	const tt = "${lastName}"
	window.onload = teste;
	
	function teste()
	{
		if(tt)
		{
			window.alert(tt);
		}
		else
		{
			window.alert("N�o pegou");
		}
	};
</script>

<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>${firstName}</h1>
	<c:label value="Firt Name:"/>
	<c:property value="firstName"/><br/>
	
	<c:label value="Last Name:" />
	<c:property value="lastName"/><br/>
	
	<c:label value="Gender:" />
	<c:property value="gender"/><br/>
	
	<c:label value="Age:" />
	<c:property value="age"/><br/>
	
	<c:label value="Email:" />
	<c:property value="email"/><br/>
	
	<c:label value="Address:" />
	<c:property value="address"/><br/>
	
	<c:label value="Favorite Color: " />
	<c:property value="favoriteColor"/><br/>
	
	<c:label value="Is subscription" />
	<c:if test="subscription">
		Subscription on<br/>
	</c:if>
	<c:else>
		Subscription off<br/>
	</c:else>
	
	<c:label value="State(s): "/>
	<c:property value="state"/><br/>
</body>
</html>