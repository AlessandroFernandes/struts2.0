<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/struts-tags" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Edit Product</title>
	
	<link type="text/css" rel="stylesheet" href="../css/style.css">
	<link type="text/css" rel="stylesheet" href="./css/style.css">
</head>
<body>
	<div class="continer">
		<h1 class="center">Add product</h1>
		
		<c:form method="POST" action="updateAction">
			<c:textfield name="id" label="ID" readonly="true"/>
			<c:textfield name="product" label="Product" />
			<c:textfield name="price" label="Price" />
			<c:submit value="submit"/>
		</c:form>
		
	</div>
</body>
</html>