<%@page import="org.apache.struts2.components.Include"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="/struts-tags" %>

<!DOCTYPE html>
<script>
	window.onload = teste;
	
	function teste(){
		if(${teste})
			{
			return alert('pegou');
			}
		return alert('n�o pegou')
		};
</script>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Welcome</title>
		
		<link type="text/css" rel="stylesheet" href="../css/style.css">
		<link type="text/css" rel="stylesheet" href="./css/style.css">
	</head>
<body>

	
	
	<div class="container">
	<%@include file="../template/header.jsp" %>
	
		<h1 class="center" >Welcome</h1>

		<a  class="center" href="/strutsteste/page/addProduct.jsp">
			<button>Add product</button>
		</a>
		
		<div class="main-table">
			<table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Product</th>
						<th>Price</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				<tbody>
					<c:iterator value="products" var="product">
						<tr>
							<td>
								<c:property value="#product.id"/>
							</td>
							<td>
								<c:property value="#product.product"/>
							</td>
							<td>
								<c:property value="#product.price"/>
							</td>
							<td>
								<a href="updateProductDataAction?id=<c:property value="#product.id"/>">
									<button>Update</button>
								</a>
							</td>
							<td>
								<a href="deleteProduct?id=<c:property value="#product.id"/>">
									<button>Delete</button>
								</a>
							</td>
						<tr>
					</c:iterator>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>