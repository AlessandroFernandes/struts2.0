<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="/struts-tags" %>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Login</title>
		<link type="text/css" rel="stylesheet" href="../css/style.css">
		<link type="text/css" rel="stylesheet" href="./css/style.css">
	</head>
<body>
<div class="login">
	<c:form method="POST" action="login" class="login__input">
		
		<c:textfield label="Login" name="username" requiredLabel="username" />
		<c:textfield label="Password" name="pass" requiredLabel="pass"/>
		<br/>
		<c:submit value="Login"/>
		
	</c:form>
</div>


</body>
</html>