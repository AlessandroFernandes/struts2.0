<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="/struts-tags" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Add product</title>
		
		<link type="text/css" rel="stylesheet" href="../css/style.css">
		<link type="text/css" rel="stylesheet" href="./css/style.css">
	</head>
<body>
	<div class="continer">
	<%@include file="../template/header.jsp" %>
	
		<h1 class="center">Add product</h1>
	
		<c:form method="POST" action="addProduct">
			<c:textfield name="product" label="Product: "/>
			<c:textfield name="price" label="Price: "/>
			<c:submit value="Submit"/>
		</c:form>
	
	</div>
</body>
</html>