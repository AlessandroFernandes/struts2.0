package Model;

public class Login {

	private String username;
	private String pass;
	
	public Login(String _username, String _pass)
	{
		this.username = _username;
		this.pass = _pass;
	}
	
	public Login() {};
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}

}
