package Model;

public class Products {

	private Integer id;
	private String product;
	private Double price;
	
	public Products(Integer _id, String _product, Double _price)
	{
		this.id = _id;
		this.product = _product;
		this.price = _price;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getproduct() {
		return product;
	}
	public void setproduct(String product) {
		this.product = product;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
}
