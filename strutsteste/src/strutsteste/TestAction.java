package strutsteste;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

import Model.Products;

public class TestAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private String gender;
	private Integer age;
	private String email;
	private String address;
	private String favoriteColor;
	private List<String> colors;
	private Boolean subscription;
	private String state;
	private List<String> manyState;
	private List<Products> products;
	
	public List<Products> getProducts() {
		return products;
	}

	public void setProducts(List<Products> products) {
		this.products = products;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<String> getManyState() {
		return manyState;
	}

	public void setManyState(List<String> manyState) {
		this.manyState = manyState;
	}

	public Boolean getSubscription() {
		return subscription;
	}

	public void setSubscription(Boolean subscription) {
		this.subscription = subscription;
	}

	public List<String> getColors() {
		return colors;
	}

	public void setColors(List<String> colors) {
		this.colors = colors;
	}

	public String getFavoriteColor() {
		return favoriteColor;
	}

	public void setFavoriteColor(String favoriteColor) {
		this.favoriteColor = favoriteColor;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String execute()
	{
		System.out.println("execute() method called");
		return SUCCESS;
	}
	
	public String formLoadAction()
	{
		this.initializeColors();
		this.initializeState();
		this.initializeProducts();
		return "input";
	}
	
	public void initializeColors()
	{
		colors = new ArrayList<String>();
		colors.add("Black");
		colors.add("White");
		colors.add("Blue");
		colors.add("Red");
	}
	
	public void initializeState()
	{
		manyState = new ArrayList<String>();
		manyState.add("Sao Paulo");
		manyState.add("Rio Janeiro");
		manyState.add("Curitiba");
	}
	
	public void initializeProducts()
	{
		products = new ArrayList<Products>();
		products.add(new Products(001, "TV", 1000.00));
		products.add(new Products(002, "Computer", 4500.00));
		products.add(new Products(003, "Hat", 20.00));
		products.add(new Products(004, "Book", 10.00));
	}

	
	/*public void validate() 
	{
		if(firstName.equals(""))
		{
			addFieldError("firstName", "First name is required");
		}
		
		if(lastName.equals(""))
		{
			addFieldError("lastName", "Last name is required");
		}
		
		if(age.equals(null))
		{
			addFieldError("age", "Age is required");
		}
		
		if(age <= 18) 
		{
			addFieldError("age", "Age should be above 18");
		}
		
	}*/
}
