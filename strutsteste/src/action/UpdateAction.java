package action;

import java.sql.SQLException;

import com.opensymphony.xwork2.ActionSupport;

import DAO.ProductDAO;
import Model.Products;

public class UpdateAction extends ActionSupport {
	
	private Integer id;
	private String product;
	private Double price;
	
	public Boolean updateProduct()
	{
		int n = 0;
		ProductDAO productDAO = new ProductDAO();
		try {
			n = productDAO.updateProduct(new Products(id, product, price));
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		if(n == 0)
		{
			return false;
		}
		return true;
	}
	
	public String execute()
	{
		boolean response = this.updateProduct();
		if(!response)
		{
			return ERROR;
		}		
		return SUCCESS;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}


}
