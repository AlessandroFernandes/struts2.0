package action;

import java.sql.SQLException;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

import DAO.LoginDAO;
import Model.Login;

public class LoginAction extends ActionSupport {
	
	private String username;
	private String pass;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public String execute() throws ClassNotFoundException, SQLException
	{
		LoginDAO loginDao = new LoginDAO();
		boolean user = loginDao.getLogin(new Login(username, pass));
		ServletActionContext.getRequest().getSession().setAttribute("user", username );
		if(user == false)
		{
			return INPUT;
		}
		return SUCCESS;
	}

}
