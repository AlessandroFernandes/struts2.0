package action;

import java.sql.SQLException;

import com.opensymphony.xwork2.ActionSupport;

import DAO.ProductDAO;
import Model.Products;

public class AddProductAction extends ActionSupport {

	private String product;
	private Double price;
	
	private int addProduct()
	{
		int successProduct = 0;
		ProductDAO addProduct = new ProductDAO();
		try {
			successProduct = addProduct.addProduct(new Products(null, product, price));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return successProduct;
	}
	
	public String execute()
	{
		int success = addProduct();
		
		if(success == 0)
		{
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
}
