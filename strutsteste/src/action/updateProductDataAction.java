package action;

import java.sql.SQLException;

import com.opensymphony.xwork2.ActionSupport;

import DAO.ProductDAO;
import Model.Products;

public class updateProductDataAction extends ActionSupport {

	private String id;
	private String product;
	private Double price;
	
	public String execute() throws ClassNotFoundException, SQLException
	{
		ProductDAO productDAO = new ProductDAO();
		Products productData = productDAO.getProductById(id);
		id = Integer.toString(productData.getId());
		product = productData.getproduct();
		price = productData.getPrice();
		return SUCCESS;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}

}
