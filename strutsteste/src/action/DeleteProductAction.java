package action;

import java.sql.SQLException;

import com.opensymphony.xwork2.ActionSupport;

import DAO.ProductDAO;

public class DeleteProductAction extends ActionSupport {

	private String id;
	private String product;
	private Double price;
	
	private void deleteProduct()
	{
		ProductDAO product = new ProductDAO();
		try {
			product.DeleteProduct(id);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String execute()
	{
		this.deleteProduct();
		return SUCCESS;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	
}
