package action;

import java.sql.SQLException;
import java.util.List;

import com.opensymphony.xwork2.ActionSupport;

import DAO.ProductDAO;
import Model.Products;

public class WelcomeAction extends ActionSupport {

	List<Products> products;
	
	private void GetAllProducts() 
	{
		ProductDAO productDAO = new ProductDAO();
		try {
			products = productDAO.getProducts();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public String execute()
	{
		teste = true;
		this.GetAllProducts();
		return SUCCESS;
	}
	
	public List<Products> getProducts() {
		return products;
	}

	public void setProducts(List<Products> products) {
		this.products = products;
	}
	
	private boolean teste;

	public boolean getTeste() {
		return teste;
	}

	public void setTeste(boolean teste) {
		this.teste = teste;
	}
}
