package factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionTest {

	public static Connection connectionString() throws SQLException, ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager
				   .getConnection("jdbc:mysql://localhost:3306/virtual_store?useTimezone=true&serverTimezone=UTC","root","");
	}
}
