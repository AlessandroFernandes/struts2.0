package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Model.Products;
import factory.ConnectionTest;

public class ProductDAO {

	
	public List<Products> getProducts() throws ClassNotFoundException, SQLException
	{
		List<Products> products = new ArrayList<Products>();
		
		new ConnectionTest();
		Connection con = ConnectionTest.connectionString();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM products");
		while(rs.next())
		{
			Products product = new Products(rs.getInt("id"), rs.getString("product"), rs.getDouble("price"));
			products.add(product);
		}
		
		con.close();
		return products;
	}
	
	public int addProduct(Products product) throws ClassNotFoundException, SQLException
	{
		int response = 0;
		
		new ConnectionTest();
		Connection con = ConnectionTest.connectionString();
		PreparedStatement ps = con.prepareStatement("INSERT INTO products(product, price) VALUES (?, ?)");
		ps.setString(1, product.getproduct());
		ps.setDouble(2, product.getPrice());
		response = ps.executeUpdate();
		
		return response;
	}
	
	public Products getProductById(String id) throws ClassNotFoundException, SQLException
	{
		Products product = null;
		new ConnectionTest();
		Connection con = ConnectionTest.connectionString();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM products WHERE id = ?");
		ps.setString(1, id);
		ResultSet rs = ps.executeQuery();
		while(rs.next())
		{
			product = new Products(rs.getInt("id"), rs.getString("product"), rs.getDouble("price"));
		}
		return product;
	}
	
	public int updateProduct(Products product) throws ClassNotFoundException, SQLException
	{
		new ConnectionTest();
		Connection con = ConnectionTest.connectionString();
		PreparedStatement ps = con.prepareStatement("UPDATE products SET product = ?, price = ? WHERE id = ?");
		ps.setString(1, product.getproduct());
		ps.setDouble(2, product.getPrice());
		ps.setInt(3, product.getId());
		int i = ps.executeUpdate();
		return i;
	}
	
	public void DeleteProduct(String id) throws ClassNotFoundException, SQLException
	{
		new ConnectionTest();
		Connection con = ConnectionTest.connectionString();
		PreparedStatement ps = con.prepareStatement("DELETE FROM products WHERE id = ?");
		ps.setString(1, id);
		int i =ps.executeUpdate();
	}
}
