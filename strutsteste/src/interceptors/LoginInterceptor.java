package interceptors;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

import action.LoginAction;

public class LoginInterceptor implements Interceptor {

	@Override
	public void destroy() {
		System.out.println("destroy() loginInterceptorcalled");
		
	}

	@Override
	public void init() {
		System.out.println("init() loginInterceptor called");
		
	}

	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		Object obj = ServletActionContext.getRequest().getSession().getAttribute("user");
		if(obj == null)
		{
			if(ai.getAction().getClass().equals(LoginAction.class))
			{
				return ai.invoke();
			}
			return "input";
		}
		return ai.invoke();
	}

}
