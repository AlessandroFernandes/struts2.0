package interceptors;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.util.ValueStack;

public class AgeGroupInterceptor implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String intercept(ActionInvocation arg0) throws Exception {
		
		ValueStack stack = arg0.getStack();
		Integer age = Integer.getInteger(stack.findString("age"));
		//String age2 = (String) stack.findValue("age");
		String ageClassification;
		if(age.equals(null))
			age = 0;
		
		if(age <= 12)
		{
			ageClassification = "kid";
		}
		else if(age >= 12 && age <= 18)
		{
			ageClassification = "young";
		}
		else
		{
			ageClassification = "adult";
		}
		
		stack.set("age", age + "(" + ageClassification + ")");
		
		String result = arg0.invoke();
;		return result;
	}

}
